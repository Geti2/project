﻿using System.Data.SqlClient;
using Interfaces.IUserRepository;
using Models.UserModel;
using Data.DataUser;
using System.Collections.Generic;

namespace Repositories.UserRepository
{
    public class UserRepository : IUserRepository
    {
        private string connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=D:\ОСН прог\2\Project\DataBaces\DataBaces\MainDatabase.mdf;Integrated Security=True";

        public bool RegisterUser(User user)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {


                sqlConnection.Open();
                SqlCommand sqlCommandCheck = new SqlCommand("SELECT login from [Users] where login = @login", sqlConnection);
                sqlCommandCheck.Parameters.AddWithValue("login", user.Login);
                sqlCommandCheck.ExecuteNonQuery();
                SqlDataReader reader = sqlCommandCheck.ExecuteReader();
                if (!reader.HasRows)
                {
                    reader.Close();
                    SqlCommand sqlCommand = new SqlCommand("INSERT INTO [Users] (name, last_name , W_M , login ,password,genus, role) VALUES(@name, @last_name , @W_M , @login , @password, @genus, @role)", sqlConnection);
                    sqlCommand.Parameters.AddWithValue("name", user.Name);
                    sqlCommand.Parameters.AddWithValue("last_name", user.LastName);
                    sqlCommand.Parameters.AddWithValue("W_M", user.W_M);
                    sqlCommand.Parameters.AddWithValue("login", user.Login);
                    sqlCommand.Parameters.AddWithValue("password", user.Password);
                    sqlCommand.Parameters.AddWithValue("genus", user.Genus);
                    sqlCommand.Parameters.AddWithValue("role", "User");

                    sqlCommand.ExecuteNonQuery();
                    reader.Close();
                    sqlConnection.Close();
                   
                    return true;


                }
                else
                {
                    sqlConnection.Close();
                    reader.Close();
                    return false;
                }
            }
        }

        public bool LoginUser(User user)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {

                sqlConnection.Open();
                SqlCommand sqlCommandCheck = new SqlCommand("SELECT login, password from [Users] where login = @login and password = @password and role = @role", sqlConnection);
                sqlCommandCheck.Parameters.AddWithValue("login", user.Login);
                sqlCommandCheck.Parameters.AddWithValue("password", user.Password);
                sqlCommandCheck.Parameters.AddWithValue("role", "User");
                sqlCommandCheck.ExecuteNonQuery();

                SqlDataReader readerCheck = sqlCommandCheck.ExecuteReader();
                if (readerCheck.HasRows)
                {
                    readerCheck.Close();


                    SqlCommand sqlCommand = new SqlCommand("SELECT Id,name,last_name, login from [Users] where login = @login", sqlConnection);
                    sqlCommand.Parameters.AddWithValue("login", user.Login);
                    sqlCommand.ExecuteNonQuery();
                    SqlDataReader reader = sqlCommand.ExecuteReader();


                    while (reader.Read())
                    {

                        
                        int Index_id = reader.GetOrdinal("Id");
                        int Index_name = reader.GetOrdinal("name");
                        int Index_last_name = reader.GetOrdinal("last_name");
                        int Index_login = reader.GetOrdinal("login");
                       

                        DataUser.Id = reader.GetInt32(Index_id);
                        DataUser.Name = reader.GetString(Index_name);
                        DataUser.LastName = reader.GetString(Index_last_name);
                        DataUser.Login = reader.GetString(Index_login);
                       

                    }
                    reader.Close();
                    readerCheck.Close();


                    return true;

                }
                else
                {

                    return false;
                }

            }
        }

        public bool LoginAdmin(User user)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                SqlCommand sqlCommandCheck = new SqlCommand("SELECT login, password from [Users] where login = @login and password = @password and role = @role", sqlConnection);
                sqlCommandCheck.Parameters.AddWithValue("login", user.Login);
                sqlCommandCheck.Parameters.AddWithValue("password", user.Password);
                sqlCommandCheck.Parameters.AddWithValue("role", "Admin");
                sqlCommandCheck.ExecuteNonQuery();

                SqlDataReader readerCheck = sqlCommandCheck.ExecuteReader();
                if (readerCheck.HasRows)
                {
                    return true;
                }
                return false;
            }
        }


            public List<string[]> UserList()
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand("Select  *  from [Users]", sqlConnection);

                SqlDataReader reader = sqlCommand.ExecuteReader();

                List<string[]> data = new List<string[]>();

                while (reader.Read())
                {
                    data.Add(new string[6]);
                    data[data.Count - 1][0] = reader[0].ToString();
                    data[data.Count - 1][1] = reader[1].ToString();
                    data[data.Count - 1][2] = reader[2].ToString();
                    data[data.Count - 1][3] = reader[3].ToString();
                    data[data.Count - 1][4] = reader[0].ToString();
                    data[data.Count - 1][5] = reader[1].ToString();
                    
                }

                reader.Close();
                sqlConnection.Close();

                return data;
            }
        }

        public void EditUser(User user)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand("UPDATE [Users] SET name = @name, last_name = @last_name, login = @login, password = @password where id = @id", sqlConnection);
                sqlCommand.Parameters.AddWithValue("name", user.Name);
                sqlCommand.Parameters.AddWithValue("last_name", user.LastName);
                sqlCommand.Parameters.AddWithValue("login", user.Login);
                sqlCommand.Parameters.AddWithValue("password", user.Password);
                sqlCommand.Parameters.AddWithValue("id", user.Id);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
            }

        }
        public void DeleteUser(int id)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommandDB = new SqlCommand("DELETE FROM [Balances] WHERE Id = @id ", sqlConnection);
                sqlCommandDB.Parameters.AddWithValue("id", id);
                sqlCommandDB.ExecuteNonQuery();

                SqlCommand sqlCommandDP = new SqlCommand("DELETE FROM [Products] WHERE user_id = @id ", sqlConnection);
                sqlCommandDP.Parameters.AddWithValue("id", id);
                sqlCommandDP.ExecuteNonQuery();

                SqlCommand sqlCommandDU = new SqlCommand("DELETE FROM [Users] WHERE Id = @id ", sqlConnection);
                sqlCommandDU.Parameters.AddWithValue("id", id);
                sqlCommandDU.ExecuteNonQuery();


                sqlConnection.Close();
            }
        }




    }
}
