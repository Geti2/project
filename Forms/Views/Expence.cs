﻿using System;
using Repositories.ProductRepository;
using Repositories.BalanceRepository;
using System.Windows.Forms;

namespace Forms
{
    public partial class Expence : Form
    {
        public Expence()
        {
            InitializeComponent();

            BalanceRepository balanceRepository = new BalanceRepository();
            ProductRepository productRepository = new ProductRepository();

            BalanceText.Text = balanceRepository.SumBalance() - productRepository.PriceBalance() + " ";
            SumPrice.Text = productRepository.PriceBalance() + " ";

            foreach (string[] s in productRepository.ShowBalanceProduct())
            DataSource.Rows.Add(s);
        }

        private void Edit_Click(object sender, EventArgs e)
        {
            EditExpence editExpence = new EditExpence();
            Hide();
            editExpence.Show();
            Close();
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            MainUser mainUser = new MainUser();
            Hide();
            mainUser.Show();
            Close();
        }

        private void Add_Click(object sender, EventArgs e)
        {
            AddExpence addExpence = new AddExpence();
            Hide();
            addExpence.Show();
            Close();
        }

      
    }
}
