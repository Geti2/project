﻿using System;
using System.Windows.Forms;
using Data.DataBalance;
using Forms.Views;

namespace Forms
{
    public partial class Balance : Form
    {
        public Balance()
        {
            InitializeComponent();
            MainText.Text = DataBalance.Main + " руб";
            StashText.Text = DataBalance.Stash + " руб";
            AccText.Text = DataBalance.Accumulation + " руб";
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            MainUser mainUser = new MainUser();
            Hide();
            mainUser.Show();
            Close();
        }

        private void Balance_Load(object sender, EventArgs e)
        {

        }

        private void Add_Click(object sender, EventArgs e)
        {
            AddBalance addBalance = new AddBalance();
            Hide();
            addBalance.Show();
            Close();
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            RemoveBalance removeBalance = new RemoveBalance();
            Hide();
            removeBalance.Show();
            Close();
        }
    }
}
