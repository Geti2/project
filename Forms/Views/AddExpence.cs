﻿using System;
using System.Windows.Forms;
using Models.ProductModel;
using Repositories.ProductRepository;
using Data.DataUser;
using Repositories.BalanceRepository;

namespace Forms
{
    public partial class AddExpence : Form
    {
        public AddExpence()
        {
            InitializeComponent();

            BalanceRepository balanceRepository = new BalanceRepository();
            ProductRepository productRepository = new ProductRepository();

            BalanceText.Text = balanceRepository.SumBalance() - productRepository.PriceBalance() + " ";
             
        }

       

        private void Exit_Click(object sender, EventArgs e)
        {
            MainUser mainUser = new MainUser();
            Hide();
            mainUser.Show();
            Close();
        }

        private void Add_Click(object sender, EventArgs e)
        {
            BalanceRepository balanceRepository = new BalanceRepository();
            ProductRepository productRepository = new ProductRepository();
            int sum = balanceRepository.SumBalance() - productRepository.PriceBalance();
            if (!string.IsNullOrEmpty(PriceText.Text) && !string.IsNullOrEmpty(NameProduct.Text))
            {
                try
                {
                    if (checkBox.Checked == false && sum < 0)
                    {
                        MessageBox.Show("Вы должны запланировать доход т.к на балансе нет средств", "Error");

                        CleanForm();
                    }
                    else
                    {

                        AddExpence addExpence = new AddExpence();

                        Product product = new Product();
                        product.IdUser = DataUser.Id;
                        product.Name = NameProduct.Text;
                        product.Price = Convert.ToInt32(PriceText.Text);

                        if (checkBox.Checked == true)
                        {
                            product.State = "Запланированный";
                        }
                        else if (checkBox.Checked == false)
                        {
                            product.State = "Незапланированный";
                        }

                        productRepository.AddProduct(product);

                        CleanForm();
                        MessageBox.Show("Вы добавили продукт","Add");

                    }
                }
                catch (Exception)
                {
                    CleanForm();
                    MessageBox.Show("Ошибка! Вы не можете добавить данное значение, пожалуста перепроверьте ваше значение! Вы можете добавить только число","Error");
                }
            }
            else
            {
                CleanForm();
                MessageBox.Show("Вы не можете оставить пустые поля", "Error");
            }
           
        }

       

        private void CleanForm()
        {
            foreach (var c in this.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).Text = String.Empty;
                }
            }
        }
    }
}
