﻿using System;
using System.Windows.Forms;
using Models.UserModel;
using Repositories.BalanceRepository;
using Data.DataBalance;

namespace Forms
{
    public partial class AddBalance : Form
    {
        public AddBalance()
        {
            InitializeComponent();
        }


        private void Add_Click(object sender, EventArgs e)
        {
           
           
                User user = new User();
                BalanceRepository balanceRepository = new BalanceRepository();

            if (!string.IsNullOrEmpty(IncomeText.Text))
            {
                try
                {
                    

                    if (radioButton1.Checked)
                    {
                        DataBalance.Main = Int32.Parse(IncomeText.Text);
                        balanceRepository.СhangeBalanceMain();
                    }
                    else if (radioButton2.Checked)
                    {
                        DataBalance.Stash = Int32.Parse(IncomeText.Text);
                        balanceRepository.СhangeBalanceStash();
                    }
                    else if(radioButton3.Checked)
                    {
                        DataBalance.Accumulation = Int32.Parse(IncomeText.Text);
                        balanceRepository.СhangeBalanceAccumulation();

                    }

                    MessageBox.Show("Вы обновили ваш доход!", "Add");
                    CleanForm();
                   
                }
                catch (Exception)
                {
                    CleanForm();
                    MessageBox.Show("Ошибка! Вы не можете добавить данное значение, пожалуста перепроверьте ваше значение! Вы можете добавить только число","Error");
                }
                finally
                {
                    CleanForm();
                }
            }
            else
            {
                MessageBox.Show("Пустая строка! Введите данные","Error");
            }







        }

        private void Exit_Click(object sender, EventArgs e)
        {
            Balance balance = new Balance();
            Hide();
            balance.Show();
            Close();
        }

        private void AddBalance_Load(object sender, EventArgs e)
        {

        }


        private void CleanForm()
        {
            foreach (var c in this.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).Text = String.Empty;
                }
            }
        }



       
    }
}
