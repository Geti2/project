﻿using System;
using System.Windows.Forms;
using Repositories.BalanceRepository;
using Data.DataBalance;


namespace Forms
{
    public partial class ShowBalance : Form
    {
       
        public ShowBalance()
        {
            InitializeComponent();
            BalanceRepository balanceRepository = new BalanceRepository();
            Income.Text = DataBalance.Main + " руб";
            SumIncome.Text = balanceRepository.SumBalance() + "руб";

            foreach (string[] s in balanceRepository.ShowBalanceUsers())
            DataSource.Rows.Add(s);
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            MainUser mainUser = new MainUser();
            Hide();
            mainUser.Show();
            Close();
        }

       
    }
}
