﻿using System;
using System.Windows.Forms;
using Data.DataUser;


namespace Forms
{
    public partial class MainUser : Form
    {
        public MainUser()
        {
            InitializeComponent();
            Login.Text = "Привет "+ DataUser.Login;
            

        }

        private void Exit_Click(object sender, EventArgs e)
        {

            MainFamily mainFamily = new MainFamily();
            Hide();
            mainFamily.Show();
            Close();
        }




        private void Balance_Click(object sender, EventArgs e)
        {
            Balance balance = new Balance();
            Hide();
            balance.Show();
            Close();
        }

        private void AddExpence_Click(object sender, EventArgs e)
        {
            AddExpence addExpence = new AddExpence();
            Hide();
            addExpence.Show();
            Close();
        }

        private void Expence_Click(object sender, EventArgs e)
        {
            Expence expence = new Expence();
            Hide();
            expence.Show();
            Close();
        }

        private void Show_All_Income_Click(object sender, EventArgs e)
        {
            ShowBalance showBalance = new ShowBalance();
            Hide();
            showBalance.Show();
            Close();
        }

        private void MainUser_Load(object sender, EventArgs e)
        {

        }

       
    }
}
