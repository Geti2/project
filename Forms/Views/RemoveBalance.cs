﻿using System;
using System.Windows.Forms;
using Repositories.BalanceRepository;

namespace Forms.Views
{
    public partial class RemoveBalance : Form
    {
        public RemoveBalance()
        {
            InitializeComponent();
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            Balance balance = new Balance();
            Hide();
            balance.Show();
            Close();
        }

        private void RemoveButton_Click(object sender, EventArgs e)
        {
            BalanceRepository balanceRepository = new BalanceRepository();

            if (radioButton1.Checked)
            {
                balanceRepository.RemoveBalanceMain(); 
            }
            else if (radioButton2.Checked)
            {
                balanceRepository.RemoveBalanceStash();
            }
            else
            {
                balanceRepository.RemoveBalanceAccumulation();
            }

            MessageBox.Show("Вы успешно удалили!","Remove");

        }
    }
}
