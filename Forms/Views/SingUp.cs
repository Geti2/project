﻿using System;
using System.Windows.Forms;
using Models.UserModel;
using Repositories.UserRepository;
using Repositories.BalanceRepository;

namespace Forms
{
    public partial class SingUp : Form
    {
        public SingUp()
        {
            InitializeComponent();
        }
        

        private void Sing_Click(object sender, EventArgs e)
        {

            if (PasswordText.Text.Length < 6)
            {
                if (string.IsNullOrWhiteSpace(NameText.Text) || string.IsNullOrWhiteSpace(LastNameText.Text)
                    || string.IsNullOrWhiteSpace(LoginText.Text) || string.IsNullOrWhiteSpace(PasswordText.Text))
                {
                    CleanForm();
                    MessageBox.Show("Введите обязательные поля");
                }
                else
                {
                    if (PasswordText.Text.Equals(Password2Text.Text))
                    {
                        User user = new User();

                        if (Man.Checked)
                        {
                            user.W_M = "Мужской";
                        }
                        else
                        {
                            user.W_M = "Женский";
                        }
                        user.Name = NameText.Text;
                        user.LastName = LastNameText.Text;
                        user.Login = LoginText.Text;
                        user.Password = PasswordText.Text;
                        user.Genus = comboBox1.Text;

                        UserRepository userRepository = new UserRepository();
                        if (userRepository.RegisterUser(user))
                        {
                            BalanceRepository balanceRepository = new BalanceRepository();
                            balanceRepository.RegBalance();
                            MessageBox.Show("Вы зарегистрировались");
                        }
                        else
                        {
                            MessageBox.Show("Такой логин уже существует");
                        }

                        CleanForm();


                    }
                    else
                    {

                        CleanForm();
                        MessageBox.Show("Неверное подтверждение пароля, попробуйте ещё раз");
                    }



                }
            }
            else
            {
                CleanForm();
                MessageBox.Show("Пароль не должен содержать больше 6 симвалов");
            }

            }

        private void Back_Click(object sender, EventArgs e)
        {
            Hide();
            MainFamily main = new MainFamily();
            main.Show();
        }



        private void CleanForm()
        {
            foreach (var c in this.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).Text = String.Empty;
                }
            }
        }

        private void SingUp_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'mainDatabaseDataSet.Genus' table. You can move, or remove it, as needed.
            this.genusTableAdapter.Fill(this.mainDatabaseDataSet.Genus);

        }
    }
}
