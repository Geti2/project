﻿namespace Forms
{
    partial class MainFamily
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainFamily));
            this.SingUp = new System.Windows.Forms.Button();
            this.SingIn = new System.Windows.Forms.Button();
            this.Exit = new System.Windows.Forms.Button();
            this.Admin = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // SingUp
            // 
            this.SingUp.BackgroundImage = global::Forms.Properties.Resources.fam;
            this.SingUp.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SingUp.Location = new System.Drawing.Point(215, 180);
            this.SingUp.Name = "SingUp";
            this.SingUp.Size = new System.Drawing.Size(200, 60);
            this.SingUp.TabIndex = 1;
            this.SingUp.Text = "Регистрация";
            this.SingUp.UseVisualStyleBackColor = false;
            this.SingUp.Click += new System.EventHandler(this.SingUp_Click);
            // 
            // SingIn
            // 
            this.SingIn.BackColor = System.Drawing.Color.YellowGreen;
            this.SingIn.BackgroundImage = global::Forms.Properties.Resources.fam;
            this.SingIn.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SingIn.Location = new System.Drawing.Point(517, 180);
            this.SingIn.Name = "SingIn";
            this.SingIn.Size = new System.Drawing.Size(200, 60);
            this.SingIn.TabIndex = 0;
            this.SingIn.Text = "Войти";
            this.SingIn.UseVisualStyleBackColor = false;
            this.SingIn.Click += new System.EventHandler(this.SingIn_Click);
            // 
            // Exit
            // 
            this.Exit.BackColor = System.Drawing.Color.YellowGreen;
            this.Exit.BackgroundImage = global::Forms.Properties.Resources.fam;
            this.Exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Exit.Location = new System.Drawing.Point(699, 491);
            this.Exit.Name = "Exit";
            this.Exit.Size = new System.Drawing.Size(200, 60);
            this.Exit.TabIndex = 2;
            this.Exit.Text = "Выход";
            this.Exit.UseVisualStyleBackColor = false;
            this.Exit.Click += new System.EventHandler(this.Exit_Click);
            // 
            // Admin
            // 
            this.Admin.BackColor = System.Drawing.Color.YellowGreen;
            this.Admin.BackgroundImage = global::Forms.Properties.Resources.fam;
            this.Admin.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Admin.Location = new System.Drawing.Point(22, 462);
            this.Admin.Name = "Admin";
            this.Admin.Size = new System.Drawing.Size(200, 105);
            this.Admin.TabIndex = 3;
            this.Admin.Text = "Войти как админ";
            this.Admin.UseVisualStyleBackColor = false;
            this.Admin.Click += new System.EventHandler(this.Admin_Click);
            // 
            // MainFamily
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Forms.Properties.Resources.fam;
            this.ClientSize = new System.Drawing.Size(942, 588);
            this.Controls.Add(this.Admin);
            this.Controls.Add(this.Exit);
            this.Controls.Add(this.SingIn);
            this.Controls.Add(this.SingUp);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainFamily";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button SingUp;
        private System.Windows.Forms.Button SingIn;
        private System.Windows.Forms.Button Exit;
        private System.Windows.Forms.Button Admin;
    }
}

