﻿
namespace Data.DataBalance
{
    public static class DataBalance
    {
        public static int Main { get; set; }

        public static int Stash { get; set; }

        public static int Accumulation { get; set; }
    }
}
